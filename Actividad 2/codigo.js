var btn1 = document.getElementById("btn1");
var btn2 = document.getElementById("btn2");
var btn3 = document.getElementById("btn3");
var btn4 = document.getElementById("btn4");
var error = "Error";

var resultado = document.getElementById("resultado")

var inputUno = document.getElementById("input-uno");
var inputDos = document.getElementById("input-dos");


btn1.addEventListener("click", function() {
    var n1 = inputUno.value;
    var n2 = inputDos.value;

    if (n1 % 1 == 0 && n2 % 1 == 0) {
        if (isNaN(suma(n1, n2))) {
            resultado.innerHTML = error.fontcolor("red");
        } else {

            resultado.innerHTML = suma(n1, n2);
        }

    } else {
        resultado.innerHTML = error.fontcolor("red");
    }
});

btn2.addEventListener("click", function() {
    var n1 = inputUno.value;
    var n2 = inputDos.value;

    if (n1 % 1 == 0 && n2 % 1 == 0) {
        if (isNaN(resta(n1, n2))) {
            resultado.innerHTML = error.fontcolor("red");
        } else {

            resultado.innerHTML = resta(n1, n2);
        }
    } else {
        resultado.innerHTML = error.fontcolor("red");
    }

});

btn3.addEventListener("click", function() {
    var n1 = inputUno.value;
    var n2 = inputDos.value;

    if (n1 % 1 == 0 && n2 % 1 == 0) {


        if (isNaN(multiplicacion(n1, n2))) {
            resultado.innerHTML = error.fontcolor("red");
        } else {

            resultado.innerHTML = multiplicacion(n1, n2);
        }
    } else {
        resultado.innerHTML = error.fontcolor("red");
    }
});

btn4.addEventListener("click", function() {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    if (n1 % 1 == 0 && n2 % 1 == 0) {
        if (isNaN(division(n1, n2))) {
            resultado.innerHTML = error.fontcolor("red");
        } else {
            if (n1 == 0 || n2 == 0) {
                resultado.innerHTML = error.fontcolor("red");
            } else {
                resultado.innerHTML = division(n1, n2);
            }

        }
    } else {
        resultado.innerHTML = error.fontcolor("red");
    }
});


function validarNumeros(n1, n2) {
    if (isNaN(n1) || isNaN(n2)) {
        return false;
    } else {
        return true
    }

}

function validarEnteros(n1, n2) {
    if (isInteger(n1) && isInteger(n2)) {
        return true;
    } else {
        return false;
    }


}


function suma(n1, n2) {
    return parseInt(n1) + parseInt(n2);
}

function resta(n1, n2) {
    return parseInt(n1) - parseInt(n2);
}

function multiplicacion(n1, n2) {
    return parseInt(n1) * parseInt(n2);
}

function division(n1, n2) {
    return parseInt(n1) / parseInt(n2);
}